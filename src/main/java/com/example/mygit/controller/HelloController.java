package com.example.mygit.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Hello")
public class HelloController {
	
	@GetMapping
	public String hello() {
		return "Hello Git fetch&diff&merge Test";
	}
	
	@PostMapping
	public void insert(String value) {
		System.out.println(value);
		System.out.println("test");
	}
	

}
