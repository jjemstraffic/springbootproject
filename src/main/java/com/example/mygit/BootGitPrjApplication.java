package com.example.mygit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootGitPrjApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootGitPrjApplication.class, args);
	}

}
